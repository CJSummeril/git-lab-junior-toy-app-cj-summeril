class SubgroupsController < ApplicationController
  before_action :logged_in_user, only: [:create]
  before_action :allowed_user, only: [:create]

  def new
    @group = Group.find(params[:group_id])
    @sub_group = @group.sub_groups.build
  end

  def create
    @sub_group = current_user.groups.build(group_params)
    if @sub_group.save
      flash[:success] = "Subgroup created!"
      redirect_to @sub_group
    else
      render 'subgroups/new', status: :unprocessable_entity
    end
  end

  private

  def group_params
    params.require(:group).permit(:name, :description, :owner_id, :parent_group_id)
  end

  def allowed_user
    return unless params[:group_id]
    return if params[:group_id] && (current_user?(Group.find(params[:group_id]).owner) || current_user.admin?)

    flash[:danger] = "You are not allowed to perform this action"
    redirect_to(root_url, status: :unauthorized)
  end
end
