module API
  module V1
    module Defaults
      extend ActiveSupport::Concern

      included do
        prefix :api
        version 'v1', using: :path
        default_format :json
        format :json
        content_type :json, 'application/json'

        helpers do
          def permitted_params
            @permitted_params ||= declared(params, include_missing: false)
          end

          def private_token
            params[:private_token]
          end

          def current_user
            @current_user ||= find_user_from_access_token
          end

          def find_user_from_access_token
            return unless private_token
            PersonalAccessToken.find_by(value: private_token)&.user
          end

          def authenticated_as_admin!
            authenticate!
            forbidden! unless current_user.admin?
          end

          def authenticate!
            unauthorized! unless current_user
          end

          def unauthorized!(reason = nil)
            render_api_error_with_reason!(401, '401 Unauthorized', reason)
          end

          def logger
            Rails.logger
          end

          # error helpers

          def render_api_error_with_reason!(status, message, reason)
            message = [message]
            message << "- #{reason}" if reason
            render_api_error!(message.join(' '), status)
          end

          def render_validation_error!(model, status = 400)
            if model.errors.any?
              render_api_error!(model.errors.messages || '400 Bad Request', status)
            end
          end

          def forbidden!(reason = nil)
            render_api_error_with_reason!(403, '403 Forbidden', reason)
          end

          def render_api_error!(message, status)
            error!({ 'message' => message }, status, header)
          end
        end

        rescue_from ActiveRecord::RecordNotFound do |e|
          error_response(message: e.message, status: 404)
        end

        rescue_from ActiveRecord::RecordInvalid do |e|
          error_response(message: e.message, status: 422)
        end
      end
    end
  end
end
