class PersonalAccessToken < ApplicationRecord
  TOKEN_LENGTH = 16
  belongs_to :user
  validates :name, presence: true, length: { maximum: 50 }
  validates :value, presence: true, length: { minimum: TOKEN_LENGTH }

  default_scope -> { order(created_at: :desc) }

  class << self
    def generate
      SecureRandom.hex(TOKEN_LENGTH)
    end
  end
end
