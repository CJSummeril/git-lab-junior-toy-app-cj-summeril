require 'grape'

module API
  module Entities
    class Group < Grape::Entity
      expose :id, documentation: { type: 'integer', example: 1 }
      expose :name, documentation: { type: 'string', example: 'Group name' }
      expose :description, documentation: { type: 'string', example: 'Group description' }
      expose :owner_id, documentation: { type: 'integer', example: 1 }
      expose :parent_group_id, documentation: { type: 'integer', example: 2 }
      expose :web_url, documentation: { type: 'string', example: 'https://example.com/groups/1' } do |group|
        GitlabJunior::Application.routes.url_helpers.group_url(group)
      end
    end
  end
end
