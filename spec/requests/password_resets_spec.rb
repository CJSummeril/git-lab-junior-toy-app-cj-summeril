require 'rails_helper'

RSpec.describe 'PasswordResets', type: :request do
  describe 'POST /password_resets' do
    let(:user) { create(:user) }

    it 'does not succeed when email is invalid' do
      post password_resets_path, params: { password_reset: { email: '' } }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(flash).not_to be_empty
    end

    it 'succeeds when email is valid but not found' do
      post password_resets_path, params: { password_reset: { email: 'random_email@example.com' } }
      expect(response).to have_http_status(:unprocessable_entity)
      expect(flash).not_to be_empty
    end

    it 'succeeds when email is valid and found' do
      post password_resets_path, params: { password_reset: { email: user.email } }
      expect(response).to have_http_status(:redirect)
      expect(flash).not_to be_empty
      expect(response).to redirect_to(root_url)
    end
  end

  describe 'POST /password_resets/id/edit' do
    let!(:user) { create(:user) }

    before do
      post password_resets_path,
        params: { password_reset: { email: user.email } }
      @reset_user = @controller.instance_variable_get(:@user)
    end

    it 'does not succeed when email is invalid' do
      get edit_password_reset_path(@reset_user.reset_token, email: "")
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_url)
    end

    it 'does not succeed with valid email but invalid token' do
      get edit_password_reset_path('invalid_token', email: @reset_user.email)
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_url)
    end

    it 'succeeds with valid email and valid token' do
      get edit_password_reset_path(@reset_user.reset_token, email: @reset_user.email)
      expect(response).to have_http_status(:success)
      expect(response.body).to include('<h1>Reset password</h1>')
    end
  end

  describe 'PATCH /password_resets/id/update' do
    let!(:user) { create(:user) }

    before do
      post password_resets_path,
        params: { password_reset: { email: user.email } }
      @reset_user = @controller.instance_variable_get(:@user)
    end

    it 'does not succeed with an invalid password and confirmation' do
      patch password_reset_path(@reset_user.reset_token),
        params: { email: @reset_user.email,
          user: { password: 'foobaz',
            password_confirmation: 'barquux' } }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it 'does not succeed with an empty password and confirmation' do
      patch password_reset_path(@reset_user.reset_token),
        params: { email: @reset_user.email,
          user: { password: '',
            password_confirmation: '' } }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it 'succeeds with a valid password and confirmation' do
      patch password_reset_path(@reset_user.reset_token),
        params: { email: @reset_user.email,
          user: { password: 'foobaz',
            password_confirmation: 'foobaz' } }
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(user_path(@reset_user))
    end
  end
end
