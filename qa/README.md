# End-to-end tests documentation

The end-to-end tests use RSpec, Capybara and webdriver. 

## Getting started

Make sure you have the Rails application up and running. Follow the instructions provided in
the [application readme](../README.md) to bring up the application.

Next, head over to the `qa` directory:

```shell
$ cd qa
```

Install the required gems:

```shell
$ bundle
```

### Running the end-to-end tests

Use the following command to run all end-to-end tests:

```shell
bundle exec rspec qa/specs/features
```

To run a single spec:

```shell
bundle exec rspec qa/specs/features/user/signup_spec.rb
```
