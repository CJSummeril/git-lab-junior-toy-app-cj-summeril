module QA
  module Support
    module API
      class Client
        attr_reader :address, :user

        AuthorizationError = Class.new(RuntimeError)

        def initialize(address: QA::Runtime::Env.app_url, personal_access_token: nil, is_new_session: true, user: nil)
          @address = address
          @personal_access_token = personal_access_token
          @is_new_session = is_new_session
          @user = user
        end

        # Personal access token
        #
        # It is possible to set the environment variable QA_ACCESS_TOKEN
        # to use a specific access token rather than create one from the UI
        # unless a specific user has been passed
        #
        # @return [String]
        def personal_access_token
          @personal_access_token ||= if user.nil?
                                       Runtime::Env.personal_access_token ||= create_personal_access_token
                                     else
                                       create_personal_access_token
                                     end

          @personal_access_token
        end

        private

        # Create PAT
        #
        # @return [String]
        def create_personal_access_token
          signed_in_initially = Page::Home.perform(&:signed_in?)

          token = Resource::PersonalAccessToken.fabricate! do |pat|
            pat.user = user
          end.token

          # If this is a new session, that tests that follow could fail if they
          # try to sign in without starting a new session.
          # Also, if the browser wasn't already signed in, leaving it
          # signed in could cause tests to fail when they try to sign
          # in again. For example, that would happen if a test has a
          # before(:context) block that fabricates via the API, and
          # it's the first test to run so it creates an access token
          #
          # Sign out so the tests can successfully sign in
          Flow::Login.sign_out if @is_new_session || !signed_in_initially

          token
        end
      end
    end
  end
end
