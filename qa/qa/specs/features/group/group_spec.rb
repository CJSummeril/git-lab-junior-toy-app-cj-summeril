require_relative '../../spec_helper'

RSpec.describe 'Group' do

  before do
    QA::Flow::Login.sign_in
  end

  it 'can be created via the UI' do
    QA::Page::Home.perform(&:click_new_top_level_group_link)

    @group = QA::Resource::Group.fabricate_via_browser_ui!

    expect(page).to have_text(@group.name)
  end

  after do
    @group&.remove_via_api!
  end
end
