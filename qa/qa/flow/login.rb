module QA
  module Flow
    module Login
      extend self

      def sign_in(as: QA::Runtime::User.default)
        Page::Home.perform(&:click_login_link)

        Page::Login.perform do |login|
          login.fill_email_field(as.email)
          login.fill_password_field(as.password)
          login.click_login_button

          raise QA::Resource::Errors::InconsistentStateError unless login.has_text?('Logged in successfully')
        end
      end

      def sign_out
        return unless Page::Home.perform(&:signed_in?)

        Page::Home.perform do |home|
          home.click_account_dropdown_link
          home.click_logout_link
        end
      end

      def sign_in_unless_signed_in(user:)
        singed_in_as_user = Page::Home.perform do |home|
          home.signed_in_as_user?(user)
        end
        unless singed_in_as_user
          sign_out
          sign_in(as: user)
        end
      end
    end
  end
end
